FROM public.ecr.aws/bitnami/node:14-prod

WORKDIR /usr/src/app
 

LABEL project="Atlantia DEV"

COPY package*.json /

COPY . .

RUN npm install -g npm
RUN npm install


EXPOSE 3000
 
CMD ["npm","run","start"]