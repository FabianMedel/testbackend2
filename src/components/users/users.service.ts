import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { InjectRepository } from '@nestjs/typeorm';
import { Model } from 'mongoose';
import { Repository } from 'typeorm';
import { UserDto } from './dto/user.dto';
import { User, UserDocument } from './schemas/user.schema';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>
  ) {}
  async create(createUserDto: UserDto) {
    try {
      const user = await new this.userModel(createUserDto).save();
      return {
        statusCode: HttpStatus.OK,
        user
      }
    } catch (error) {
      return {
        statusCode: HttpStatus.BAD_REQUEST,
        error: error.massage
      }
    }
  }

  async findAll() {
    try {
      const users = await this.userModel.find().exec();
      return {
        statusCode: HttpStatus.OK,
        users
      }
    } catch (error) {
      return {
        statusCode: HttpStatus.BAD_REQUEST,
        error: error.message,
      }
    }
  }
}
