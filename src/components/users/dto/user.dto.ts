import { ApiProperty } from '@nestjs/swagger';

export class UserDto {
    @ApiProperty({
        required: true,
        nullable: false,
        example: 'Rubén',
        type: String,
      })
    readonly name: String;

    @ApiProperty({
        required: true,
        nullable: false,
        example: '27',
        type: String,
      })
    readonly age: String;

    @ApiProperty({
        required: true,
        nullable: false,
        example: 'Estado de México',
        type: String,
      })
    readonly city: String;
}
