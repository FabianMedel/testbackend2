import { DocumentBuilder } from '@nestjs/swagger';

export const swaggerConfig = new DocumentBuilder()
  .setTitle('Atlantia Test')
  .setDescription('')
  .setVersion('1.0')
  .build();