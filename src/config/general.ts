import * as dotenv from 'dotenv';

dotenv.config();

const { env } = process;

export const config = () => ({
  port: parseInt(env.PORT) || 3000,
  host: env.HOST || 'localhost'
});