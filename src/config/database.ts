import { registerAs } from '@nestjs/config';
import * as dotenv from 'dotenv';
import { join } from 'path';

dotenv.config();
const { env } = process;

export const databaseConfig = registerAs('database', () => ({
  main: {
    url: env.URL_DATABASE || 'mongodb+srv://admin:pQo82ujIYNptFBxG@cluster0.dzxo9.mongodb.net/atlantiaDev?retryWrites=true&w=majority',
  },
}));
