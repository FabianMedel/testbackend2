import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from "./components/users/users.module";
import { config, databaseConfig } from './config';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [config, databaseConfig],
      cache: false,
      isGlobal: true,
    }),
    MongooseModule.forRoot('mongodb+srv://admin:pQo82ujIYNptFBxG@cluster0.dzxo9.mongodb.net/atlantiaDev?retryWrites=true&w=majority'),
    UsersModule
  ],
})
export class AppModule {}
